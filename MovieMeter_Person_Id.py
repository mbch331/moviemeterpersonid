import pywikibot
from pywikibot import pagegenerators as pg
import urllib
from urllib import request
import re
from datetime import date

SparqlQuery = 'SELECT ?item ?itemLabel WHERE {  ?item wdt:P1969 ?MovieMeter_identificatiecode_voor_regisseur.  FILTER NOT EXISTS { ?item wdt:P9463 ?mmpid }} LIMIT 100'
site=pywikibot.Site('wikidata','wikidata')
repo=site.data_repository()

def wd_sparql_generator(query):
  wikidatasite=pywikibot.Site('wikidata','wikidata')
  generator=pg.WikidataSPARQLPageGenerator(query,site=wikidatasite)
  for wd in generator:
    wd.get(get_redirect=True)
    yield wd

def getData(wikidataitem):
    if wikidataitem.exists() :
        mmd = 'https://www.moviemeter.nl/director/' + wikidataitem.claims.get(u'P1969')[0].getTarget().title()
        mmpRequest = request.Request(mmd,None,
            {
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
            }
        )
        mmp=''
        mmpUrl=request.urlopen(mmpRequest).geturl()
        print(mmpUrl)
        try:
            mmp = re.search(r"https://www.moviemeter.nl/personen/(\d+)/", mmpUrl).group(1)
            setMMPClaim(wikidataitem, mmp, mmd)
        except Exception as e:
            print (e)
        finally:
            print(mmp)

def setMMPClaim(item,value,url):
    claim = pywikibot.Claim(repo, u'P9463') #Correct property needs to be created
    claim.setTarget(value)
    statedin = pywikibot.Claim(repo, u'P248')
    mm = pywikibot.ItemPage(repo, u'Q2158761')
    statedin.setTarget(mm)
    checked=pywikibot.Claim(repo, u'P813')
    today=date.today()
    dateChecked=pywikibot.WbTime(year=int(today.strftime("%Y")), month=int(today.strftime("%m")), day=int(today.strftime("%d")))
    checked.setTarget(dateChecked)
    sourceurl=pywikibot.Claim(repo, u'P854')
    sourceurl.setTarget(url)
    claim.addSources([statedin, checked, sourceurl], summary='Add Source for MovieMeter Person Id')
    item.addClaim(claim,summary='Add MovieMeter Person Id from MovieMeter Director Id')

def main():
    for wdBot in wd_sparql_generator(SparqlQuery):
       getData(wdBot)
       exit()

main()